#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.base_test_01 import BaseTest01
from TestData.Eddie_test_data import HomePageData
import time
import pytest

class Test01(BaseClass):
    def test_login_eddie(self, getData):
        tc01 = BaseTest01(self.driver)
        # logger
        log = self.getLogger()
        tc01.define_page()
        tc01.input_value().send_keys(getData["input"])

    @pytest.fixture(params=HomePageData.test_01_base)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param