#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import pytest
from selenium import webdriver
import time


driver = None

def pytest_addoption(parser):
    parser.addoption(
        "--browser_name", action="store", default="chrome"
    )
    parser.addoption(
        "--browser_version", action="store", default="81"
    )
    parser.addoption(
        "--os", action="store", default="Windows 10"
    )
    parser.addoption(
        "--build_info", action="store", default="Automation test"
    )

# ----------------------------------------------------------------------
# Fixture: Read the LambdaTest config file
# ----------------------------------------------------------------------

@pytest.fixture
def lt_config(scope='session'):
    # Read the config file
    with open('lt_config.json') as config_file:
        config = json.load(config_file)

    # Verify authentication config
    assert 'authentication' in config
    assert 'username' in config['authentication']
    assert 'key' in config['authentication']

    # Return the config data
    return config

@pytest.fixture
def setup(lt_config, request):
    # Console Input
    browser_name = request.config.getoption("--browser_name")
    browser_version = request.config.getoption("--browser_version")
    os_type = request.config.getoption("--os")
    build_info_text = request.config.getoption("--build_info")
    # Concatenate the URL
    username = lt_config['authentication']['username']
    key = lt_config['authentication']['key']
    url = f"http://{username}:{key}@hub.lambdatest.com/wd/hub"

    # Request a remote browser from LambdaTest
    caps = {}

    browser = {
        "browserName": browser_name,
        "build": build_info_text,
        "console": True,
        "network": True,
        "platform": os_type,
        "version": browser_version,
        "video": True,
        "visual": True
    }

    caps.update(browser)
    caps['name'] = request.node.name
    driver = webdriver.Remote(command_executor=url, desired_capabilities=caps)

    # Make its calls wait up to 30 seconds for elements to appear
    driver.implicitly_wait(30)
    driver.maximize_window()
    request.cls.driver = driver
    yield driver
    # Create a finalizer for the remote browser
    def finalizer():
        if request.node.rep_call.failed:
            driver.execute_script("lambda-status=failed")
        else:
            driver.execute_script("lambda-status=passed")
        driver.quit()

    # Add the finalizer to the test
    request.addfinalizer(finalizer)

# ----------------------------------------------------------------------
# Hook: Set the test result
# ----------------------------------------------------------------------

@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
  # This sets the result as a test attribute for LambdaTest reporting.
  # Execute all other hooks to obtain the report object.
  outcome = yield
  rep = outcome.get_result()

  # Set an report attribute for each phase of a call, which can
  # be "setup", "call", "teardown"
  setattr(item, "rep_" + rep.when, rep)




