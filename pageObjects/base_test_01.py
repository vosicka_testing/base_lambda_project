#!/usr/bin/python
# -*- coding: utf-8 -*-


from selenium.webdriver.common.by import By


class BaseTest01:

    def __init__(self, driver):
        self.driver = driver

    url = "https://google.com"
    username = (By.CLASS_NAME, "gLFyf")

    def define_page(self):
        return self.driver.get(self.url)

    def input_value(self):
        return self.driver.find_element(*BaseTest01.username)